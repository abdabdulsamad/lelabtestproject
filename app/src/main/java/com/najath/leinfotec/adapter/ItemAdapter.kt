package com.najath.leinfotec.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.najath.leinfotec.databinding.ItemListBinding
import com.najath.leinfotec.roomdb.ListEntity


class ItemAdapter(
    private var context: Context,
    private var itemclick: Itemclick


) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    private var listBean: List<ListEntity?>? = null

    fun setDataList(
        listBean: List<ListEntity?>? = null,
        context: Context?,

        ) {
        this.listBean = listBean

        this.context = context!!

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding =
                ItemListBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val bean = listBean?.get(position)
        (holder as ViewHolder).bindData(itemclick,bean!!)


    }


    override fun getItemCount(): Int {
        if (!listBean.isNullOrEmpty()) {
            return listBean!!.size
        }
        return 0    }
    class ViewHolder(private var binding: ItemListBinding) :
            RecyclerView.ViewHolder(binding.root) {




        fun bindData(itemclick: Itemclick, entity: ListEntity?) {
            binding.data = entity
            binding.listeners=itemclick

        }


    }
}