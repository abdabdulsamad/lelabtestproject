package com.najath.leinfotec.network;


import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.najath.leinfotec.pojomodel.ResponseData
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory

import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*


const val URL_VERSION = ""
var BASE_URL = "https://jsonplaceholder.typicode.com/"


/**
 * Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
 * full Kotlin compatibility.
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

/**
 * Use the Retrofit builder to build a retrofit object using a Moshi converter with our Moshi
 * object.
 */


var okHttpClient: OkHttpClient = UnsafeOkHttpClients.getUnsafeOkHttpClient()




private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .addCallAdapterFactory(CoroutineCallAdapterFactory())
    .client(okHttpClient)
    .baseUrl(BASE_URL)
    .build()


interface ApiService {

    @GET("users")
    fun getApiData(
    ): Deferred<Response<List<ResponseData>?>>?
}

/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object RetrofitApi {
    val retrofitService: ApiService by lazy { retrofit.create(ApiService::class.java) }
}

