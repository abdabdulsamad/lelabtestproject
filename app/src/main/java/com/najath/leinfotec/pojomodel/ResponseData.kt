package com.najath.leinfotec.pojomodel
import com.squareup.moshi.JsonClass

import com.squareup.moshi.Json
@JsonClass(generateAdapter = true)
data class ResponseData(
    @Json(name = "address")
    var address: Address,
    @Json(name = "company")
    var company: Company,
    @Json(name = "email")
    var email: String,
    @Json(name = "id")
    var id: Int,
    @Json(name = "name")
    var name: String,
    @Json(name = "phone")
    var phone: String,
    @Json(name = "username")
    var username: String,
    @Json(name = "website")
    var website: String
) {
    @JsonClass(generateAdapter = true)
    data class Address(
        @Json(name = "city")
        var city: String,
        @Json(name = "geo")
        var geo: Geo,
        @Json(name = "street")
        var street: String,
        @Json(name = "suite")
        var suite: String,
        @Json(name = "zipcode")
        var zipcode: String
    ) {
        @JsonClass(generateAdapter = true)
        data class Geo(
            @Json(name = "lat")
            var lat: String,
            @Json(name = "lng")
            var lng: String
        )
    }

    @JsonClass(generateAdapter = true)
    data class Company(
        @Json(name = "bs")
        var bs: String,
        @Json(name = "catchPhrase")
        var catchPhrase: String,
        @Json(name = "name")
        var name: String
    )
}