package com.najath.leinfotec.pojomodel
import androidx.room.Database
import androidx.room.RoomDatabase
import com.najath.leinfotec.roomdb.ListEntity
import com.najath.leinfotec.roomdb.ListEntityDao


@Database(entities = [ListEntity::class], version = 1)

abstract class AppDatabase : RoomDatabase() {
    abstract fun categoryDao(): ListEntityDao?
}