package com.najath.leinfotec.roomdb

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
class ListEntity :Serializable {
    @PrimaryKey(autoGenerate = true)
    public var id = 0

    @ColumnInfo(name = "email")
    public var email: String? = null
    @ColumnInfo(name = "givenid")
    var givenid: Int=0
    @ColumnInfo(name = "name")
    public var name: String? = null
    @ColumnInfo (name = "phone")
    var phone: String?=null


    @ColumnInfo(name = "username")
    var username:String?=null

    @ColumnInfo(name = "website")
    var website: String?=null

    @ColumnInfo(name = "city")
    var city: String?=null

    @ColumnInfo(name = "geolat")
    var geolat: String?=null

    @ColumnInfo(name = "geolong")
    var geolong: String?=null
    @ColumnInfo(name = "street")
    var street: String?=null
    @ColumnInfo(name = "suite")
    var suite: String?=null
    @ColumnInfo(name = "zipcode")
    var zipcode: String?=null
    @ColumnInfo(name = "bs")
    var bs: String?=null
    @ColumnInfo(name = "catchPhrase")
    var catchPhrase: String?=null
    @ColumnInfo(name = "compName")
    var compName: String?=null


}