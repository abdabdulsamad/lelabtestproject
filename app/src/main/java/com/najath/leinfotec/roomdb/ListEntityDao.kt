package com.najath.leinfotec.roomdb

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ListEntityDao {

    @Query("SELECT * FROM ListEntity order by id")
    fun getAll(): List<ListEntity?>?
    @Query("SELECT * FROM ListEntity where givenid=:id")
    fun getOne(id:Int): List<ListEntity?>?


    @Insert
    fun insert(task: ListEntity?)

}