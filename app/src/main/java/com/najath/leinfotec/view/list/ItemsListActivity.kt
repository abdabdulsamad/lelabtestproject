package com.najath.leinfotec.view.list

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.najath.leinfotec.R
import com.najath.leinfotec.adapter.ItemAdapter
import com.najath.leinfotec.adapter.Itemclick
import com.najath.leinfotec.databinding.ActivityItemListBinding
import com.najath.leinfotec.view.product.ItemView
import kotlinx.android.synthetic.main.activity_item_list.*

class ItemsListActivity : AppCompatActivity(),Itemclick {

    private val viewModel: ItemsListViewModel by lazy {
        ViewModelProviders.of(this).get(ItemsListViewModel::class.java)

    }
    lateinit var listProductsAdapter:ItemAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityItemListBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_item_list)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        viewModel.setActivity(this)
        initAdapter()
        setProductsAdapter()
    }


        private fun initAdapter() {

            listProductsAdapter =
                    ItemAdapter(this, this)

            // vertical and cycle layout
            val layoutManager =
                    GridLayoutManager(this, 1)
            layoutManager.orientation= LinearLayoutManager.VERTICAL
            expandable_recycler_view.layoutManager = layoutManager
            expandable_recycler_view.setHasFixedSize(true)
            expandable_recycler_view.itemAnimator = DefaultItemAnimator()
            expandable_recycler_view.adapter = listProductsAdapter
            //for content based height
            expandable_recycler_view.isNestedScrollingEnabled = false
        }

        private fun setProductsAdapter() {
            viewModel.response.observe(this) {

                        listProductsAdapter.setDataList(
                                it,
                                this
                        )

            }
        }



    override fun itmclick(id: Int) {
        val intent=Intent(this, ItemView::class.java)
        intent.putExtra("id", id);

        startActivity(intent)    }


}