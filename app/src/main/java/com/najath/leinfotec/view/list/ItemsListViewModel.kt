package com.najath.leinfotec.view.list

import android.app.Activity
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.najath.leinfotec.pojomodel.DatabaseClient
import com.najath.leinfotec.roomdb.ListEntity
import kotlinx.coroutines.*

class ItemsListViewModel (application: Application) : AndroidViewModel(application) {
    private lateinit var activity: Activity
    lateinit  var db : DatabaseClient
    // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _responseString = MutableLiveData<String>()

    // The internal MutableLiveData String that stores the most recent response
    private var _response = MutableLiveData< List<ListEntity?>>()

    // The external immutable LiveData for the response String
    val response: LiveData< List<ListEntity?>>
        get() = _response


    fun setActivity(activity: Activity) {
        this.activity = activity
        db= DatabaseClient.getInstance(activity)!!
        getdata()
    }


    private fun getdata() {
        GlobalScope.launch {

           val data= db.appDatabase.categoryDao()!!.getAll()!!

            activity.runOnUiThread{
                _response.value=data
            }

            //  tvTotalQty.text = data.size.toString() + " " + getString(R.string.nos)





        }
    }

}