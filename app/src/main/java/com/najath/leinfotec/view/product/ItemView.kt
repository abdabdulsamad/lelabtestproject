package com.najath.leinfotec.view.product

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.najath.leinfotec.R
import com.najath.leinfotec.databinding.ActivityItemViewBinding

class ItemView  : AppCompatActivity() {

    private val viewModel: ItemViewModel by lazy {
        ViewModelProviders.of(this).get(ItemViewModel::class.java)

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityItemViewBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_item_view)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this
        viewModel.setActivity(this)

        viewModel.response.observe(this) {


            binding.data=it

        }



    }
}