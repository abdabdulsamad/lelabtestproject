package com.najath.leinfotec.view.product

import android.app.Activity
import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.najath.leinfotec.pojomodel.DatabaseClient
import com.najath.leinfotec.roomdb.ListEntity
import kotlinx.coroutines.*

class ItemViewModel (application: Application) : AndroidViewModel(application) {
    private lateinit var activity: Activity

    lateinit  var db : DatabaseClient
    // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    // The internal MutableLiveData String that stores the most recent response
    private var _response = MutableLiveData< ListEntity?>()

    // The external immutable LiveData for the response String
    val response: LiveData<ListEntity?>
        get() = _response


     var  id: Int = 0
    fun setActivity(activity: Activity) {
        this.activity = activity
        id= activity.getIntent().getIntExtra("id",0)
        db= DatabaseClient.getInstance(activity)!!
        getdata(id)
    }

    private fun getdata(id:Int) {
        GlobalScope.launch {

            val data= db.appDatabase.categoryDao()!!.getOne(id)!!

            activity.runOnUiThread{
                _response.value=data.get(0)
            }

            //  tvTotalQty.text = data.size.toString() + " " + getString(R.string.nos)





        }
    }
}