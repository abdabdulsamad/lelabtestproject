package com.najath.leinfotec.view.splash

import android.app.Activity
import android.app.Application
import android.content.Intent
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.najath.leinfotec.network.AppPreference
import com.najath.leinfotec.network.RetrofitApi
import com.najath.leinfotec.pojomodel.DatabaseClient
import com.najath.leinfotec.pojomodel.ResponseData
import com.najath.leinfotec.roomdb.ListEntity
import com.najath.leinfotec.view.list.ItemsListActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.concurrent.thread

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private lateinit var activity: Activity
    lateinit  var db :DatabaseClient
    // Create a Coroutine scope using a job to be able to cancel when needed
    private var viewModelJob = Job()

    // the Coroutine runs using the Main (UI) dispatcher
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)

    private val _responseString = MutableLiveData<String>()

    // The internal MutableLiveData String that stores the most recent response
    private val _response = MutableLiveData<List<ResponseData>>()

    // The external immutable LiveData for the response String
    val response: LiveData<List<ResponseData>>
        get() = _response


    fun setActivity(activity: Activity) {
        this.activity = activity
        db= DatabaseClient.getInstance(activity)!!
        AppPreference.init(activity)
        if(AppPreference.isLogin){
            activity.startActivity(Intent(activity,ItemsListActivity::class.java))

        }else{
            getdata()
        }
    }



    /**
     * Sets the value of the response LiveData to the Mars API status or the successful number of
     * Mars properties retrieved.
     */
    fun getdata() {

        coroutineScope.launch {

            // Get the Deferred object for our Retrofit request
            val getPropertiesDeferred =
                RetrofitApi.retrofitService.getApiData()
            try {
                // Await the completion of our Retrofit request
                val result = getPropertiesDeferred!!.await()


                when (result.code()) {
                    200 -> {

                        _response.value = result.body()

                        val results=_response.value
                        thread {
                            results?.forEach {

                                val catClass= ListEntity()
                                catClass.givenid=it.id
                                catClass.name=it.name
                                catClass.email=it.email
                                catClass.phone=it.phone
                                catClass.username=it.username
                                catClass.website=it.website
                                catClass.city=it.address.city
                                catClass.geolat=it.address.geo.lat
                                catClass.geolong=it.address.geo.lng
                                catClass.street=it.address.street
                                catClass.suite=it.address.suite
                                catClass.zipcode=it.address.zipcode
                                catClass.bs=it.company.bs
                                catClass.catchPhrase=it.company.catchPhrase
                                catClass.compName=it.company.name

                                db.appDatabase.categoryDao()?.insert(catClass)
                            }
                       }
                        AppPreference.isLogin=true
                        activity.startActivity(Intent(activity,ItemsListActivity::class.java))
                    }
                    401 -> {

                    }
                    else -> {

                    }
                }

            } catch (e: Exception) {

                e.printStackTrace()
            }
        }
    }
}